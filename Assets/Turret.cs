﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

    private Transform target;
    [Header("Attributes")]
    public float range = 15f;
    public string enemyTag = "Heretic";
    public Transform partToRotate;
    public float turnSpeed = 10f;
    public float firerate = 1f;
    private float fireCountdown = 0f;
    public GameObject bulletPrefab;
    public Transform firePoint;
    void Start() {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }
    void UpdateTarget()
    {
        GameObject[] Heretics = GameObject.FindGameObjectsWithTag("Heretic");
        float shortestDistance = Mathf.Infinity;
        GameObject nearestHeretic = null;
        foreach (GameObject Heretic in Heretics)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, Heretic.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestHeretic = Heretic;
            }
        }
        if (nearestHeretic != null && shortestDistance <= range)
        {
            target = nearestHeretic.transform;
        }
        else
        {
            target = null;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (target == null)
            return;
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        if (fireCountdown <= 0)
        {
            Shoot();
            fireCountdown = 1f / firerate;
        }
        fireCountdown -= Time.deltaTime;
	}
    void Shoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (bullet != null)
            bullet.Seek(target);
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
